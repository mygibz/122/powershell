Get-Service | ForEach-Object {
    if ($_.Status -eq "running") {
        Write-Host -f green $_.Name,$_.Status
    } else {
        Write-Host -f red $_.Name,$_.Status
    }
}