# options
$collection = Get-Service | Select-Object Status, Name

# logic
foreach ($item in $collection) {
    if ($item | Where-Object Status -eq "Stopped") {
        Write-Host ($item) -ForegroundColor "red"
    } else {
        Write-Output $item
    }
}