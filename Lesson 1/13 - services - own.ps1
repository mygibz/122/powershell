# options
$collection = Get-Service | Sort-Object Name -Descending
$searchFor = 3,4,5

# logic
$i = 1;
foreach ($item in $collection) {
    if ($searchFor.Contains($i)) {
        $item
    }
    $i++
}