$n = Read-Host "N"

if ($n -gt 0) {
    $fakultaet = 1
    while ($n -gt 1) {
        $fakultaet = $fakultaet * $n
        $n = $n - 1
    }
    Write-Host $fakultaet
} else {
    Write-Host "FEHLER"
}