$number1 = $args[0]
$number2 = $args[1]

# make sure that number 2 is greater than number 1
# swap them if needed
if ($number1 -gt $number2) {
    $number2 = $number1
    $number1 = $args[1]
}

# calculate result
[decimal]($number2 - $number1) * [decimal](Get-Random -Minimum 1 -Maximum 100)