$biggestNumber = 0

for ($i = 0; $i -lt $args.Count; $i++) {
    if ($args[$i] -gt $args[$biggestNumber]) {
        $biggestNumber = $i
    }
}

"Maximum " + $args[$biggestNumber] + " at Position $biggestNumber"