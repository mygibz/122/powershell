if ([int]($args[0] -eq "pitch") -eq (Get-Random -Minimum 0 -Maximum 2)) {
    "You won, it was " + $args[0]
} else {
    "You loose, it was " + (&{ if ($args[0] -eq "toss") { "pitch" } else { "toss" } })
}