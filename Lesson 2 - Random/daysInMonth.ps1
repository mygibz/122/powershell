$month = (Get-Date).month

switch ($args[0]) {
    'Jan' { $month = 1 }
    'Feb' { $month = 2 }
    'Mar' { $month = 3 }
    'Apr' { $month = 4 }
    'Mai' { $month = 5 }
    'Jun' { $month = 6 }
    'Jul' { $month = 7 }
    'Aug' { $month = 8 }
    'Sept' { $month = 9 }
    'Oct' { $month = 10 }
    'Nov' { $month = 11 }
    'Dez' { $month = 12 }
}

(Get-Culture).DateTimeFormat.GetMonthName($month) + " has " + [datetime]::DaysInMonth((Get-Date).year,$month) + " days"