# calculate time variable (HHmm, e.g. 1446 for 14:46/2:46pm)
[int]$time = Get-Date -Format HHmm
if ($args.length -gt 0) { $time = $args[0] -replace ":", "" }

# output correct statement based on time
switch ($time) {
    {$_ -ge 2330} { "You should be sleeping now"; break }
    {$_ -ge 2000} { "Good Night"; break }
    {$_ -ge 1800} { "Good Evening"; break }
    {$_ -ge 1200} { "Good Afternoon"; break }
    {$_ -ge 0600} { "Good Morning"; break }
    Default { "Hello" }
}