<#
.synopsis
Lists installed applications

.description
Lists installed applications, by listing every application that could be uninstalled

.notes
Get-ItemProperty:  Get's a registry entry
Format-Table:      Outputs the data in a formatted table, in order to make it look better
#>

# List all items, that are uninstallable (and therefor installed) and 
Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Format-Table -AutoSize