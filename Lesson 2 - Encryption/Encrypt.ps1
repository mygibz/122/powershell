$key = Get-Content 'key.ini'

# $original = Read-Host 'Input string'
$original = Get-Content 'io/decrypted'

$output = ''

foreach ($letter in [char[]]$original) {
    # check if it's a space
    if ($letter -ne ' ') {
        # define original coordinate
        [int]$i = "abcdefghijklmnopqrstuvwxyz".IndexOf($letter)

        # set output
        $output += $key[$i]
    } else {
        $output += ' '
    }
}

Set-Content 'io/encrypted' $output