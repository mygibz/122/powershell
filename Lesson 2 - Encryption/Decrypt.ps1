$key = Get-Content 'key.ini'

# $original = Read-Host 'Input string'
$original = Get-Content 'io/encrypted'

$output = ''

foreach ($letter in [char[]]$original) {
    # check if it's a space
    if ($letter -ne ' ') {
        # define original coordinate
        [int]$i = $key.IndexOf($letter)

        # set output
        $output += "abcdefghijklmnopqrstuvwxyz"[$i]
    } else {
        $output += ' '
    }
}

Set-Content 'io/decrypted' $output