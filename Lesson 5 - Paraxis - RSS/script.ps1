<#
.synopsis
Gets the RSS feed from my website, and outputs it.

.description
Gets the RSS feed from my website using an Invoke-WebRequest, and outputs it using the Format-Table Function.

.notes
Invoke-WebRequest:  Get's the content of a file (aka: like the curl command)
Format-Table:       Outputs the data in a formatted table, in order to make it look better
#>


# Get content from website
$content = [xml](Invoke-WebRequest "https://ant.lgbt/posts/index.xml")

# Uncomment, if network error, to use local file
# $content = [xml]$Content = Get-Content 'example.rss'

# Output title of Channel
Write-Host $content.rss.channel.title

# Output variables of each item
$content.rss.channel.item | foreach-object {
    new-object psobject -Property @{
        PostTitle = $_.title
        PostDate = Get-Date -Format "dd.MM.yyyy" $_.pubDate
    }
} | Format-Table PostTitle,PostDate

pause