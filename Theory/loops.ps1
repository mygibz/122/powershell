"For loop"

for ($i = 0; $i -lt 5; $i++) {
    $i
}

""
"Forr loop"

for ($i = 5; $i -ge 0 ; $i--) {
    $i
}

""
"While loop:"

$t = 1
while ($t -le 10) {
    $t
    $t += 1
}