# options
$folder = "files"
$templateFile = "Copyright.ini"

# read vars
[string]$extension = Read-Host "Dateiendungen mit SPACE getrennt angeben (e.g. '*.cpp')"


# do magic
foreach ($file in (Get-ChildItem "$folder/$extension")) {
    Set-Content $file.FullName ((Get-Content $templateFile) + "`n" + (Get-Content $file.FullName))
}
