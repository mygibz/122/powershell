#include <iostream>


// Variablen definieren
int auswahl = 4;
int anzahlVehikel = 0;

int anzahlAutos = 0;
int anzahlBus = 0;
int anzahlRad = 0;


int main()
{
	printf_s("1 Auto\n");
	printf_s("2 Lastwagen oder Reisebus\n");
	printf_s("3 Fahrrad oder Motorrad\n");
	printf_s("0 Fahrzeugeingabe beenden");
	
	// Eingabe
	for (; auswahl != 0; anzahlVehikel++)
	{
		printf_s("\n-> ");
		scanf_s("%d", &auswahl);

		// IF-Abfragen um die verschiedenen Eingabemöglichkeiten zu verarbeiten
		if (auswahl == 1)
		{
			anzahlAutos++;
			printf_s("%d. Fahrzeug = Auto", anzahlVehikel + 1);
		}
		else 
		{
			if (auswahl == 2)
			{
				anzahlBus++;
				printf_s("%d. Fahrzeug = Lastwagen oder Reisebus", anzahlVehikel + 1);
			} 
			else 
			{
				if (auswahl == 3)
				{
					anzahlRad++;
					printf_s("%d. Fahrzeug = Fahrrad oder Motorrad", anzahlVehikel + 1);
				}
				else 
				{
					// Falls nicht 1, 2 oder 3 eingegeben wurde (entweder 0 oder ungültig)
					if (auswahl != 0)
					{
						printf_s("Fehler, bitte geben Sie eine gueltige Auswahl ein");
						--anzahlVehikel; // In diesem Durchgang wurde kein Vehikel hinzugefügt
					}
					else 
					{
						// Prüfe ob mindestens ein Fahrzeug eingegeben wurde
						if (anzahlVehikel == 0)
						{
							printf_s("Fehler, bitte mindestens ein Fahrzeug eingeben");
							auswahl = 4;
							--anzahlVehikel; // In diesem Durchgang wurde kein Vehikel hinzugefügt
						}
					}
				}
			}
		}
	}

	// Verarbeite Daten
	printf("\nAnzahl eingebened Fahrzeuge:\t\t%d Fahrzeuge\n", anzahlVehikel);
	printf_s("Anteil an Autos:\t\t\t%.2f Prozent\n", (float) anzahlAutos / (float) (anzahlVehikel -1) * 100);
	printf_s("Anteil an Lastwagen oder Reisebusen:\t%.2f Prozent\n", anzahlBus / (float) (anzahlVehikel - 1) * 100);
	printf_s("Anteil an Fahrrad oder Motorrad:\t%.2f Prozent\n", anzahlRad / (float) (anzahlVehikel - 1) * 100);

	return 0;
}