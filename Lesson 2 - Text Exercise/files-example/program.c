#include <iostream>
#include <stdio.h>

int main() {
	for (int i = 32; i <= 43; i++) {
		char symbol1 = i;
		char symbol2 = i + 12;
		char symbol3 = i + 24;
		char symbol4 = i + 36;
		char symbol5 = i + 48;
		char symbol6 = i + 60;
		char symbol7 = i + 72;
		char symbol8 = i + 84;
		printf("%c  %c  %c  %c  %c  %c  %c  %c\n", symbol1, symbol2, symbol3, symbol4, symbol5, symbol6, symbol7, symbol8);
	}
}