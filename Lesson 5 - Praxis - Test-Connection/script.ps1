<#
.synopsis
Lists installed applications

.description
Lists installed applications, by listing every application that could be uninstalled

.notes
Get-ItemProperty:  Get's a registry entry
Format-Table:      Outputs the data in a formatted table, in order to make it look better
#>

foreach ($domain in "google.com","example.com","ant.lgbt") {
    Test-Connection $domain | Add-Content 'out.log'
}