"Processor:     " + $(Get-WmiObject Win32_Processor).Name
"RAM:           " + [math]::Round($(Get-WmiObject Win32_OperatingSystem).TotalVisibleMemorySize / 1MB,2) + "GB"
"Grafikkarte:   " + $(Get-WmiObject Win32_VideoController).Name
"Netzwerkkarte: " + $(Get-NetAdapter).InterfaceDescription
"Festplatte:    " + [math]::Round($(Get-PSDrive C).Used / 1GB,2) + "GB" + " / " + ([math]::Round($(Get-PSDrive C).Used / 1GB,2) + [math]::Round($(Get-PSDrive C).Free / 1GB,2)) + "GB"