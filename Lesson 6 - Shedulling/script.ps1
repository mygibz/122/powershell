$ST_A = New-ScheduledTaskAction -Execute "c:\windows\system32\notepad.exe"
$ST_T = New-ScheduledTaskTrigger -Once -At 5am

Register-ScheduledTask -TaskName "Notepad at 5am" -Action $ST_A -Trigger $ST_T