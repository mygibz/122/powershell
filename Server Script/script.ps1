<#
.synopsis
Configures a Windows Server

.description
Configures Hostname, IP, Gateway and DNS. Then disables the Firewall and installs the AD-Domain-Services and elevates the machine to a domain controller
#>

# Set Hostname
Rename-Computer -NewName $(Read-Host "Enter Hostname (e.g. MYADDC01)")

# Set IP
Remove-NetIPAddress –InterfaceAlias $((Get-NetAdapter).Name) -Confirm:$false
New-NetIPAddress –InterfaceAlias $((Get-NetAdapter).Name) –IPAddress $(Read-Host "Enter IPv4 (e.g. 10.0.0.2)") –PrefixLength $(Read-Host "Enter IP Prefix (e.g. 24)") -DefaultGateway $(Read-Host "Enter Default Gateway (e.g. 10.0.0.1)")

# Set DNS
Set-DnsClientServerAddress -InterfaceAlias $((Get-NetAdapter).Name) -ServerAddresses $(Read-Host "Enter primary DNS Server (e.g. 10.0.0.1)"), $(Read-Host "Enter secondary DNS Server (e.g. 1.1.1.1)")

# Deactivate Firewall
if ($(Read-Host "Disable the firewall ('yes' to disable, or anything else to keep it active)") -eq "yes") {
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
}

# install AD
Install-WindowsFeature AD-Domain-Services

# Elevate Machine to Domain Controller
Install-ADDSForest -DomainName $(Read-Host "Enter domain name for AD (e.g. vdom.local)") -InstallDNS