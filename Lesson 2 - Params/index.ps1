param (
    [decimal]$temperatur = 42.69,
    [string]$ort = "Zug"
)

$TemperaturF = $temperatur * 1.8 + 32
Write-Host "In $ort sind es $temperatur°C ($temperaturF°F)"